import numpy as np
import random
import re
from gurobipy import *
from matplotlib import pyplot as plt
import pydot
import graphviz
import networkx as nx
from typing import List
from time import time

from parcours import shortest_path,shortest_path_pl
from classes import Graph, Vertex, Edge

def test(index,values,params,repetition=50):
    l= []
    for i in  values:
        print(i)
        cpt = 0
        times = []
        nb_edges = 0
        while cpt < repetition:
            if(not cpt%10): print(cpt)
            if(index == 0):G = Graph.generate_graph(i,params[0],params[1],params[2],params[3])
            elif(index == 1):G = Graph.generate_graph(params[0],i,params[1],params[2],params[3])
            elif(index == 2):G = Graph.generate_graph(params[0],params[1],i,params[2],params[3])
            elif(index == 3):G = Graph.generate_graph(params[0],params[1],params[2],i,params[3])
            elif(index == 4):G = Graph.generate_graph(params[0],params[1],params[2],params[3],i)
            G.display()
            nb_edges += len(G.edges)
            vs = [j for j in G.vertices]
            random_index = random.randrange(len(vs))
            source = vs[random_index]
            vs.remove(source)
            random_index = random.randrange(len(vs))
            dest = vs[random_index]
            while(not(len(G.multi_path(len(G.vertices),source,dest)[0])) or  not(len(G.multi_path(len(G.vertices),source,dest)[1])) ):
                vs = [j for j in G.vertices]
                random_index = random.randrange(len(vs))
                source = vs[random_index]
                vs.remove(source)
                random_index = random.randrange(len(vs))
                dest = vs[random_index]
            t = time()
            time_transf = shortest_path(G,source,dest)[2]
            time_djikstra = time() -t
            t = time()
            time_transf += shortest_path_pl(G,source,dest)[1]
            time_pl = time() -t
            time_transf/=2
            cpt +=1
            times.append((time_djikstra-time_transf, time_pl-time_transf, time_transf))
        l.append((sum([j[0] for j in times])/len(times),sum([j[1] for j in times])/len(times),sum([j[2] for j in times])/len(times), nb_edges/len(times)))
    return l

def on_file(file, source, dest):
    g = Graph.parser(file)
    g.display()
    if(source in g.vertices and dest in g.vertices):
        shortest_path(g,source, dest)
    else:
        print("error in the vertex parameters")


if __name__=="__main__":
    choice = int(input("voulez\n1-Tester l'efficacité des solutions proposées\n2-Resoudre un probleme de ce type à partir d'un graphe"))
    if(choice ==1):
        index = int(input("Quel parametres voulez vous faire varier:\n1-Le nombre de sommmet\n2-La probabilité d'un arc entre deux sommets\n3-max start\n4-max duration\n5-max repetition."))
        index-=1
        if(index == 1):
            min_value = float(input("veuillez entrer la valeur minimal du parametre à faire varier:"))
            max_value = float(input("veuillez entrer la valeur maximal du parametre à faire varier:"))
            step = float(input("veuillez entrer la valeur du pas entre chaque valeur:"))
        else:
            min_value = int(input("veuillez entrer la valeur minimal du parametre à faire varier:"))
            max_value = int(input("veuillez entrer la valeur maximal du parametre à faire varier:"))
            step = int(input("veuillez entrer la valeur du pas entre chaque valeur:"))
        values= []
        tmp = min_value
        print(tmp, max_value, step)
        while(tmp < max_value):
            values.append(tmp)
            tmp+=step
        params= []
        if(index!= 0):
            params.append(int(input("Donnez la valeurs a utiliser pour le parametre 'nombre de sommet':")))
        if(index!= 1):
            params.append(float(input("Donnez la valeurs a utiliser pour le parametre 'probabilité d'un arc entre deux sommet':")))
        if(index!= 2):
            params.append(int(input("Donnez la valeurs a utiliser pour le parametre 'max start':")))
        if(index!= 3):
            params.append(int(input("Donnez la valeurs a utiliser pour le parametre 'max duration':")))
        if(index!= 4):
            params.append(int(input("Donnez la valeurs a utiliser pour le parametre 'max repetition':")))
        print(test(index,values,params))
    elif(choice ==2):
        file = input("Donnez le chemin vers le fichier contenant le graphe:")
        try:
            with open(file,"r") as f:
                source = input("Donnez le nom du sommet source")
                dest = input("Donnez le nom du sommet destination")
                on_file(f,Vertex(source),Vertex(dest))
        except FileNotFoundError:
            print("file doesn't exist")
