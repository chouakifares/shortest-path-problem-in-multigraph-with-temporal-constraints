# shortest-path-problem-in-multigraph-with-temporal-constraints 

The projects aims to solve the problem of shortest path in a multigraph containing temporal constraints. 
The problem is explicitly formulated by the article [Path Problems in Temporal Graphs](https://www.vldb.org/pvldb/vol7/p721-wu.pdf).
In this project we
1. We suggest two solutions that can solve the problem of how to get from a source vertex S to a destination vertex D in the multigraph in regard of a certain optimality criteria 
2. We offer a plateform that let's the user simulate how such a problem can be solved and compare between the two solutions that we suggested 
3. We give the possibility to the user to experiement on his own graphs and use our algorithms to find the best solution to a problem that resembles the one we're interested in. 

## Setup 
We use the python 3.8 to implement our solutions. We use networkx, graphviz and pydot to display the graphs, We use gurobipy which is the python API for the gurobi solver to solve the linear program associated with the problem. The path algorithm used to solve the problem was the Djikstra Algorithm.    

In order to run the program you will first set up a virtual environment as described in the following [link](https://docs.python.org/3/library/venv.html)

Then you'll have to install all the necessary requirements
``` 
pip -r install requirements.txt 
```
After that you'll only need to run the command 
``` 
python3 main.py
```
