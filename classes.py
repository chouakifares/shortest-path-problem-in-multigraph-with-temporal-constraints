import io
import numpy as np
import random
import re
from gurobipy import *
from matplotlib import pyplot as plt
import pydot
import graphviz
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import networkx as nx
from typing import List
from time import time

class Vertex:
    """ We use this class to modelise the several airports available
        class representing the vertices of the graph
        self.name: the name of the airport
    """
    def __init__(self, name: str):
        """ Constructor of the Vertex class
        @params:
        name (str): name of the vertex (name of the aiport)
        """
        self.name = name
    def __eq__(self, other):
        return self.name == other.name
    def __str__(self):
        return "Vertex:{}".format(self.name)
    def __repr__(self):
        return "Vertex:{}".format(self.name)

class Edge:
    """we use this class to represent the folowing situation
        a flight flying from "source" airport to "dest" airport available at "start"
        and takes "duration" day to get to get to "dest"
        class representing the Edges of the graph
        Attributs:
            self.source: the source vertex of the edge (source airport)
            self.dest: the destination vertex of the edge (destination airport)
            self.start: the date at which of the flight is available at the source airport
            self.duration: the time the flight takes to et from "source" to "dest" airport
    """
    def __init__(self, source: Vertex,dest: Vertex,start: int,duration: int):
        """Constructor of the Edge class
        @params:
            source (Vertex): the source vertex of the edge (source airport)
            dest (Vertex): the destination vertex of the edge (destination airport)
            start (int): the date at which of the flight is available at the source airport
            duration (int): the time the flight takes to et from "source" to "dest" airport
        """
        self.source = [source]
        self.dest = [dest]
        self.start = start
        self.duration = duration

    def __eq__(self, other)->bool:
        return (self.source == other.source and
            self.dest == other.dest and
            self.start == other.start and
            self.duration == other.duration)

    def __contains__(self,v: Vertex)->bool:
        return self.source[0] == v or self.dest[0] == v

    def __str__(self)-> str:
        return "Edge\nsource: {},\ndest:{},\nstarts at day : {},\ntakes {} days".format(self.source, self.dest, self.start, self.duration)

    def __repr__(self)-> str:
        return "\nEdge\nsource: {},\ndest:{},\nstarts at day : {},\ntakes {} days\n".format(self.source, self.dest, self.start, self.duration)

class Graph:
    """We use this class to represent the entirety of airports and the flights between them
        class representing the whole multigraph
        Attributs:
            self.vertices: list of all the vertices in the multigraph (list of all the airports)
            self.edges: list of all the edges in the multigraph  (list of all the flights between them)
    """

    def __init__(self, V:List[Vertex], E:List[Edge]):
        """Constructor of the graph class
        @params:
        V (list[Vertex]): list of vertices representing the airports.
            ex: [vertex("A"),vertex("B"),vertex("C"),vertex("D")]
        E (list[Edge]): list of edges representing the flights.
            ex: [Edge(A,B,8,2), Edge(A,C,3,8), Edge(C,D,4,8)]
        """
        self.edges = []
        for e in E: # ignoring the redudant edges
            if e not in self.edges:
                self.edges.append(e)

        self.vertices = []
        for v in V: # ignoring the redudant vertices
            if v not in self.vertices:
                self.vertices.append(v)

    """
        Method that takes a vertex and returns all the neighbors of that vertex
        @params:
            v: a vertex of the graph
        returns list of vertices that are neighbors if v
    """
    def predecessors(self, v:Vertex)->List[Vertex]:
        l = [i.source[0] for i in self.edges if i.dest[0] == v]
        return l
    """
        Method that takes a vertex and returns all the successors of that vertex
        @params:
            v: a vertex of the graph
        returns list of vertices that are successors if v
    """
    def successors(self, v:Vertex)->List[Vertex]:
        l = [i.dest[0] for i in self.edges if i.source[0] == v]
        return l

    """
      Method that returns all the edges between two vertices
      @params:
        source: the source vertex of the edges
        dest: the destination vertex of edges
    """
    def get_edges(self,source: Vertex,dest: Vertex):
        res = []
        for i in self.edges:
            if(i.source[0] == source and i.dest[0] ==dest):
              res.append(i)
        return res

    @staticmethod
    def suppVertex(G, v: Vertex):
        """
        Deletes a vertex v and all the edges connected to it from the graph G
        @params:
        G: Graph to modify
        v: vertex to delete
        return: a new graph G'(V',E')
        """
        new_edges = [e for e in G.edges if v not in e]
        new_vertices = [vertex for vertex in G.vertices if vertex != v]
        return Graph(new_vertices, new_edges)

    def __str__(self):
        """
        prints graph in text format
        """
        return f"""Graph:
        vertices:{self.vertices}
        edges:{self.edges}"""

    @staticmethod
    def generate_graph(n, p,max_start,max_duration, max_repetition=1):
        """
        generates a graph following the parameters given
        @params
        n: number of vertices in the graph
        p: probability of an edge between two vertices
        max_start/max_duration : maximum values that the labels ooon the edges can take
        max_repetition: maximum number of edges that can exist between two vertices
        """
        if (p <= 0) or (p >= 1):
            raise Exception("p doit être dans ]0:1[")
        #sommets
        V = [Vertex(chr(65+i))  for i in range(n)]
        #V=[ Vertex(ascii_lowercase[i]) for i in range(n) ]
        #arcs
        E = []

        for index in range(max_repetition):
            for i in range(n):
                for j in range(n):
                    #not reflexive
                    if (random.random()<=p and i!=j):
                        E.append(Edge(V[i],V[j],random.randint(1,max_start),random.randint(1,max_duration)))

        return Graph(V,E)
    #TARNSFORM

    #VERTEX
    def Ret_V_out(self,n):
      i = -1
      cpt = 0
      j=0
      V_out_t= [[] for _ in range(n)]
      for edges in self.edges:
          element =(self.edges[j].source[0].name,self.edges[j].start)
          #eviter redondences exemple element = (0,2)
          if(element[0] in [V[0][0] for V in V_out_t if V] ):
            i=[V[0][0] for V in V_out_t if V ].index(element[0])
            if element not in V_out_t[i]:
              V_out_t[i].append(element)
          else:
            V_out_t[cpt].append(element)
            cpt+=1
          j+=1

      return V_out_t

    def Ret_V_in(self,n):

      i = -1
      cpt = 0
      j=0
      V_in_t= [[] for _ in range(n)]
      for edges in self.edges:
          element =(self.edges[j].dest[0].name,self.edges[j].start+self.edges[j].duration)

          #eviter redondences exemple element = (0,2)
          if(element[0] in [V[0][0] for V in V_in_t if V] ):
            i=[V[0][0] for V in V_in_t if V ].index(element[0])
            if element not in V_in_t[i]:
              V_in_t[i].append(element)
          else:
            V_in_t[cpt].append(element)
            cpt+=1
          j+=1
      return V_in_t


    def TransVertices(self,n):
      V_in = self.Ret_V_in(n)
      V_out = self.Ret_V_out(n)
      V_del = V_in
      #in order
      ListVertixes= [[] for _ in range(n)]
      k=0
      for i in range(len(V_out)):
        for j in range(len(V_in)):
          if(V_out[i] and  V_in[j]):
            if(V_out[i][0][0]==V_in[j][0][0]):
              ListVertixes[k] = V_out[i]+V_in[j]
              V_del.remove(V_in[j])
              k+=1
              break
          else:
            #elements dans V_out et pas dans V_in
            if(V_out[i]):
              ListVertixes[k] = V_out[i]
              k+=1
              break

      #elements dans V_in et pas dans V_out
      for i in range(len(V_del)):
        if(V_del[i]):

          ListVertixes[k] = V_del[i]
          k+=1



      for i in range(n):

        ListVertixes[i] = list(dict.fromkeys(ListVertixes[i]))
        ListVertixes[i] = sorted(ListVertixes[i],key = lambda tup:tup[1])


      #Vertixes of the transformed graph
      ListVer = []
      #sommets
      for i in range(len(ListVertixes)):
        for vert_out in ListVertixes[i]:
          ListVer.append(Vertex(vert_out[0]+'_'+str(vert_out[1])))
      return ListVer

    #EDGES TRANSF
    def TransEdges(self,n):
      ListVer = self.TransVertices(n)
      ListE =[]
      #initialiser liste des arcs a 0 pour les memes

      i = 0

      while (i<=len(ListVer)-1):

        if(i!=len(ListVer)-1):
            j=i
        else :
          break

        while(ListVer[j].name.split('_')[0]==ListVer[j+1].name.split('_')[0]):
          ListE.append(Edge(ListVer[j],ListVer[j+1],0,0))
          if(j!=len(ListVer)-2):
            j+=1
          else:
            break
        i = j+1

      #Ajouter les lambda (poid de chaque arrete)

      ListETest = ListE

      for i in range(len(self.edges)):
        Bsource = 0
        j=0
        k=0

        while(Bsource==0 and j<len(ListVer)):
          start = int(ListVer[j].name.split('_')[1])
          if(self.edges[i].source[0].name==ListVer[j].name.split('_')[0] and self.edges[i].start == start):
            Bsource=1
            break

          j+=1

        Bdest = 0

        while(Bdest==0 and k < len(ListVer)):

          if(self.edges[i].dest[0].name==ListVer[k].name.split('_')[0]):

            durat_plus_start= int(ListVer[k].name.split('_')[1])
            Bsomme = ((self.edges[i].duration+self.edges[i].start) ==durat_plus_start)

            Bverif = self.edges[i].start==start

            if( Bsomme and Bverif ):
              #verfier que c'est bien dans le graphe
              if(Edge(Vertex(ListVer[j].name.split('_')[0]),Vertex(ListVer[k].name.split('_')[0]),start,self.edges[i].duration)  in self.edges):
                ListETest.append(Edge(ListVer[j],ListVer[k],self.edges[i].duration,0))
                Bdest = 1


          k+=1
        #si on a pas trouver d'arc correspondant a cette source, boucler (probleme dans le start )
        if(Bdest ==0):

          print(self.edges[i])


      #unique
      for i in range(len(ListETest)):
        unique = []
        for edge in ListETest:
          if edge in unique:
            continue
          else:
            unique.append(edge)

      ListEdges = unique
      return ListEdges

    #TRANSFORMED GRAPH
    def TransGraph(self,n):
      ListEdg = self.TransEdges(n)
      ListVer = self.TransVertices(n)
      return  Graph(ListVer,ListEdg)

    #MULTIPATH
    '''
    Multi Path
    find a path from a source vertex s to a destination vertex d in the MultiGraph
    '''
    def multi_path(self,n,s :Vertex , d:Vertex):
      V_in_ = self.Ret_V_in(n)
      V_out_ = self.Ret_V_out(n)

      out_s= []
      out_d= []

      for i in range(len(V_out_)):
        if(V_out_[i]):

          if(V_out_[i][0][0]== s.name):
            out_s+= V_out_[i]
          if(V_out_[i][0][0]==d.name):
            out_d+= V_out_[i]

      for i in range(len(V_in_)):
        if(V_in_[i]):

          if(V_in_[i][0][0]==s.name):
            out_s+= V_in_[i]

          if(V_in_[i][0][0]==d.name):
            out_d+= V_in_[i]

      #unique
      unique_s = []
      for i in range(len(out_s)):

        for e in out_s:
          if e in unique_s:
            continue
          else:
            unique_s.append(e)

      out_s = unique_s
      unique_d = []

      for i in range(len(out_d)):

        for e in out_d:
          if e in unique_d:
            continue
          else:
            unique_d.append(e)

      out_d = unique_d
      return out_s,out_d

    def display(self):
      dur = False
      edges = [(e.source[0].name, e.dest[0].name) for e in self.edges]
      for e in self.edges:
          if(e.duration!=0):
            dur = True
      G = nx.MultiDiGraph()
      for i in self.vertices:
        G.add_node(i.name)
      if(dur):
        for e in self.edges:
          G.add_edge(e.source[0].name, e.dest[0].name, label = "st : "+str(e.start) + " \ndur : " + str(e.duration))
      else:
        for e in self.edges:
          G.add_edge(e.source[0].name, e.dest[0].name, label = str(e.start))
      to_pdot = nx.drawing.nx_pydot.to_pydot(G)
      png_str = to_pdot.create_png(prog='dot')

      # treat the DOT output as an image file
      sio = io.BytesIO()
      sio.write(png_str)
      sio.seek(0)
      img = mpimg.imread(sio)

      # plot the image
      imgplot = plt.imshow(img, aspect='equal')
      plt.axis('off')
      plt.show()
    def display_path(self,path):
        dur = False
        edges = [(e.source[0].name, e.dest[0].name) for e in self.edges]
        for e in self.edges:
            if(e.duration!=0):
              dur = True
        G = nx.MultiDiGraph()
        vs = [i for i in self.vertices]
        for v in path:
          G.add_node(v, nodetype="green")
          vs = [i for i in vs if i != Vertex(v)]
        if(dur):
          for e in self.edges:
              if(e.source[0] in path and e.dest[0] in path):
                G.add_edge(e.source[0].name, e.dest[0].name, label = "st : "+str(e.start) + " \ndur : " + str(e.duration), color="r", weight=5)
              else:
                G.add_edge(e.source[0].name, e.dest[0].name, label = "st : "+str(e.start) + " \ndur : " + str(e.duration))
        else:
          for e in self.edges:
            if(e.source[0].name in path and e.dest[0].name in path):
                G.add_edge(e.source[0].name, e.dest[0].name, label = str(e.start),color="red", weight=5)
                G.add_node(e.source[0].name, color="green")
                G.add_node(e.dest[0].name, color="green")
            else:
                G.add_edge(e.source[0].name, e.dest[0].name, label = str(e.start))
        to_pdot = nx.drawing.nx_pydot.to_pydot(G)
        png_str = to_pdot.create_png(prog='dot')
         # treat the DOT output as an image file
        sio = io.BytesIO()
        sio.write(png_str)
        sio.seek(0)
        img = mpimg.imread(sio)
         # plot the image
        imgplot = plt.imshow(img, aspect='equal')
        plt.axis('off')
        plt.show()
    """
    parses a graph from an input file f
    @params
    f: input file
    returns a graph G
    """
    @staticmethod
    def parser(f):
      pattern =r"\(((\d|[a-zA-Z])+),((\d|[a-zA-Z])+),(\d+),(\d+)\)"
      lines = f.readlines()
      nb_v= int(lines[0].rstrip())
      nb_e = int(lines[1].rstrip())
      vertices = []
      try:
          for i in range(int(nb_v)):
              index = 0
              while(lines[2+i][index]==" "):
                      index+=1
              vertices.append(Vertex(lines[i+2][index:].rstrip()))
      except ValueError:
          print("Valeur incorrecte pour nombre de sommets:",nb_v)
      edges = []
      try:
          for j in range(int(nb_e)):
              index = 0
              while(lines[2+nb_v+j][index]==" "):
                  index+=1
              match = re.match(pattern, lines[2+nb_v+j][index:])
              edges.append(
                  Edge(
                      Vertex(match.group(1)),
                      Vertex(match.group(3)),
                      int(match.group(5)),int(match.group(6)))
                  )
      except ValueError:
          print("Valeur incorrecte pour nombre d aretes:",nb_e)
      return Graph(vertices,edges)
